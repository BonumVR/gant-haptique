tool

# Représente un évènement audio basique. Toutes les classes events audio héritent
class_name AudioEvent
extends Node

# Variation de pitch minimum et maximum sur le son
export(Vector2) var random_pitch = Vector2.ZERO

# Variation de volume minimum et maximum sur le son
export(Vector2) var random_volume = Vector2.ZERO

export var override_parent_pitch = false
export var override_parent_volume = false

var event

func _ready():
	randomize()
	event = self if is_audio_node(self) else get_child(0)


# Lancer l'event
func post_event():
	# Randomiser le pitch
	if random_pitch != Vector2.ZERO:
		var random = rand_range(random_pitch.x, random_pitch.y)
		event.pitch_scale += random

	# Randomiser le volume
	if random_pitch != Vector2.ZERO:
		var random = rand_range(random_volume.x, random_volume.y)
		event.volume_db += random

		event.play()



# Helper : Est-ce que le node donné est un node audio
# return bool
func is_audio_node(node : Node):
	return node is AudioStreamPlayer or \
	node is AudioStreamPlayer2D or \
	node is AudioStreamPlayer3D or \
	node.get_class() == "AudioEvent"



# Afficher les avertissements de config dans l'éditeur
func _get_configuration_warning():
	if !is_audio_node(self) && !is_audio_node(get_child(0)):
		return "Ce noeud ou son premier enfant doit être un noeud AudioStreamPlayer"
	return ""

func get_class(): return "AudioEvent"
