tool

# Event audio qui envoie aléatoirement un de ses enfants à chaque fois
class_name SequenceAudioEvent
extends "res://src/Audio/RandomAudioEvent.gd"

enum SequenceEnd {
	STOP,
	LOOP,
	REVERSE,
	REPEAT_LAST
}

export(SequenceEnd) var sequence_end = SequenceEnd.STOP

var cur_index = 0

var _is_playing = false
var _reverse = false

func _ready():
	._ready()


func reset():
	cur_index = 0
	_is_playing = false


func post_event():
	if _is_playing: return

	_children[cur_index].post_event()
	_is_playing = true

	yield(_children[cur_index], "finished")

	_is_playing = false
	cur_index += 1 if !_reverse else -1

	if cur_index < _children.size(): return

	match sequence_end:
		SequenceEnd.STOP:
			_is_playing = true
		SequenceEnd.LOOP:
			reset()
		SequenceEnd.REVERSE:
			_reverse = true
			cur_index -= 1
		SequenceEnd.REPEAT_LAST:
			cur_index -= 1
