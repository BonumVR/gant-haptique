tool

# Event audio qui envoie aléatoirement un de ses enfants à chaque fois
class_name RandomAudioEvent
extends "res://src/Audio/AudioEvent.gd"

var _children = []


# Les sons sont mis en cache au lancement
func _ready():
  ._ready()

  for child in get_children():
    if !.is_audio_node(child): return

    _children.append(child)

    if !child.override_parent_pitch:
      child.random_pitch = random_pitch

    if !child.override_parent_volume:
      child.random_volume = random_volume


# Lancer l'event
func post_event():
  var index = floor(rand_range(0, _children.size()))
  _children[index].post_event()


# Afficher les avertissements de config dans l'éditeur
func _get_configuration_warning():
  var cond = false
  for child in get_children():
    if .is_audio_node(child):
      cond = true
      break

  return "" if cond else "Au moins un des enfants de ce noeud doit être de type AudioStreamPlayer"
