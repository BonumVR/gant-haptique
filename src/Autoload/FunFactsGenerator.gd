extends Node

var data = [
	tr("Les rats-taupes nus n'invalident pas la théorie de l'évolution."),
	tr("Les wombats sont les seuls mammifères dont les selles ont la forme de cubes."),
	tr("Lorsqu'un fourmilier se sent en danger, il se place dans une position de défense en T."),
	tr("Les réfrigirateurs sont moins chers chez But."),
	tr("Vous pouvez obtenir de nouveaux fun facts pour seulement 2,99€.")
]


func get_random_fact():
	var index = floor(rand_range(0, data.size()))
	return data[index]
