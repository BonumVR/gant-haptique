extends Node


# Compare deux dictionnaires
# param strong : bool - Prendre en compte les valeurs qui n'existent pas dans dict2
# return : true si les deux dictionnaires ont les mêmes clés et que leurs valeurs sont égales
func compare_dicts(dict1 : Dictionary, dict2 : Dictionary, strong = true) -> bool:

	# Itérer sur les clés du dict
	for key in dict1.keys():
		# Si la clé n'existe pas dans l'autre
		if !dict2.get(key): return false
		# Comparer les valeurs
		if dict1[key] != dict2[key]: return false

	return true
