# Définit un craft possible
extends Node


export(Dictionary) var craft = {}

# OPTIMIZATION : Ne stocker qu'un shader et l'appliquer à un mesh déjà instancié
# Le résultat du craft
export(PackedScene) var result


func get_result():
	return get_child(0)


func get_class(): return "Craft"
