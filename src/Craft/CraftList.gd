extends Node


func _ready():
	pass


# Cherche le craft dans la liste des crafts possibles
# return : Le résultat du craft ou null
func find_craft(dict : Dictionary):
	for child in get_children():

		# Ignorer si l'enfant n'est pas un craft
		if child.get_class() != "Craft": continue

		var result = Utils.compare_dicts(dict, child.craft)
		
		if result: return child.result
		return null
