# Représente un ingrédient qui peut etre mis dans le chaudron
# devra hérité de la classe qui gère le grab des objets
extends RigidBody

export var ingredient_name = ""

func get_class(): return "CraftIngredient"
