extends Area


signal ingredient_added(type)
signal ingredient_removed(type)


var _overlapping_objects = []


# Détruit tous les ingrédients dans le chaudron
func destroy_objects():
	for o in _overlapping_objects:
		o.queue_free()


func _on_body_entered(body):
	if body.get_class() == "CraftIngredient":
		_overlapping_objects.append(body)
		emit_signal("ingredient_added", body.ingredient_name)


func _on_body_exited(body):
	if body.get_class() == "CraftIngredient" && _overlapping_objects.contains(body):
		_overlapping_objects.remove(body)
		emit_signal("ingredient_removed", body.ingredient_name)


func _on_craft_finished(success):
	destroy_objects()
