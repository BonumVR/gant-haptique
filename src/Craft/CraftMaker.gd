# Permet de crafter des objets
extends Spatial


signal craft_finished(success)


var _content = {}


func _ready():
	pass



# Ajoute un ingrédient au contenu
func _on_ingredient_added(type):
	if !_content.get(type): _content[type] = 0
	_content[type] += 1


# Retire un ingrédient du contenu
func _on_ingredient_removed(type):
	if _content.get(type):
		_content[type] -= 1 if _content[type] > 0 else 0


func _on_craft_validated():
	pass
