# Haptic Glove Game

Project description

## Download



## Installation

Clone this repo and open the project using [Godot 3.3.2](https://godotengine.org/download)

## Documentation

Project's API Reference can be found [here](https://gant-haptique.gitlab.io/gant-haptique/).
Godot docs [here](https://docs.godotengine.org/en/stable/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## License

This project has different licenses for software and assets.

Software, which includes .tscn, .gd, .tres and any file containing code, is licensed under the [GNU General Public License v3](./GPLv3-LICENSE.md)

All audio files, visual assets of this project, which include but not limited to 3D models, UI elements, sound files, 2D images, vector graphics, are distributed under [Creative Commons CC BY-SA 4.0](./CC-LICENSE.md).
To give proper credit, please use the below copypasta.

```
[LIST OF USED ASSETS] by
[PROJECT-NAME] by Clément ORLANDINI, Solène POBELLE, Simon ROZEC, Noé SIMON
Link: [LINK]
License: [LICENSE-LINK]
```
