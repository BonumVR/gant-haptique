+++
title = "CraftMaker.gd"
description = "Permet de crafter des objets"
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [Spatial](../Spatial)

## Description

Permet de crafter des objets

## Signals

- signal craft_finished(success): 
