+++
title = "AudioEvent"
description = ""
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [Node](../Node)

## Description

## Property Descriptions

### random\_pitch

```gdscript
export var random_pitch = "(0, 0)"
```

Variation de pitch minimum et maximum sur le son

### random\_volume

```gdscript
export var random_volume = "(0, 0)"
```

Variation de volume minimum et maximum sur le son

### override\_parent\_pitch

```gdscript
export var override_parent_pitch = false
```

### override\_parent\_volume

```gdscript
export var override_parent_volume = false
```

### event

```gdscript
var event
```

## Method Descriptions

### post\_event

```gdscript
func post_event()
```

Lancer l'event

### is\_audio\_node

```gdscript
func is_audio_node(node: Node)
```

Helper : Est-ce que le node donné est un node audio
return bool

### get\_class

```gdscript
func get_class()
```

