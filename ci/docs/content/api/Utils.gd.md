+++
title = "Utils.gd"
description = ""
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [Node](../Node)

## Description

## Method Descriptions

### compare\_dicts

```gdscript
func compare_dicts(dict1: Dictionary, dict2: Dictionary, strong = true) -> bool
```

Compare deux dictionnaires
param strong : bool - Prendre en compte les valeurs qui n'existent pas dans dict2
return : true si les deux dictionnaires ont les mêmes clés et que leurs valeurs sont égales