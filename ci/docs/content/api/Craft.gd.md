+++
title = "Craft.gd"
description = "Définit un craft possible"
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [Node](../Node)

## Description

Définit un craft possible

## Property Descriptions

### craft

```gdscript
export var craft = {}
```

### result

```gdscript
export var result = "[Object:null]"
```

OPTIMIZATION : Ne stocker qu'un shader et l'appliquer à un mesh déjà instancié
Le résultat du craft

## Method Descriptions

### get\_result

```gdscript
func get_result()
```

### get\_class

```gdscript
func get_class()
```

