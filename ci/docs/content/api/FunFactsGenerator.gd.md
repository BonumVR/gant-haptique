+++
title = "FunFactsGenerator.gd"
description = ""
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [Node](../Node)

## Description

## Property Descriptions

### data

```gdscript
var data
```

## Method Descriptions

### get\_random\_fact

```gdscript
func get_random_fact()
```

