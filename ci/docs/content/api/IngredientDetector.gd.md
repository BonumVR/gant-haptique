+++
title = "IngredientDetector.gd"
description = ""
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [Area](../Area)

## Description

## Method Descriptions

### destroy\_objects

```gdscript
func destroy_objects()
```

Détruit tous les ingrédients dans le chaudron

## Signals

- signal ingredient_added(type): 
- signal ingredient_removed(type): 
