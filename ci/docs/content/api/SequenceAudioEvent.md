+++
title = "SequenceAudioEvent"
description = ""
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

## Enumerations

### SequenceEnd

```gdscript
const SequenceEnd: Dictionary = {"LOOP":1,"REPEAT_LAST":3,"REVERSE":2,"STOP":0}
```

## Property Descriptions

### sequence\_end

```gdscript
export var sequence_end = 0
```

### cur\_index

```gdscript
var cur_index
```

## Method Descriptions

### reset

```gdscript
func reset()
```

### post\_event

```gdscript
func post_event()
```

