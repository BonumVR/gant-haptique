+++
title = "CraftIngredient.gd"
description = "Représente un ingrédient qui peut etre mis dans le chaudron\ndevra hérité de la classe qui gère le grab des objets"
author = "developer"
date = "2021-07-08"
+++

<!-- Auto-generated from JSON by GDScript docs maker. Do not edit this document directly. -->

**Extends:** [RigidBody](../RigidBody)

## Description

Représente un ingrédient qui peut etre mis dans le chaudron
devra hérité de la classe qui gère le grab des objets

## Property Descriptions

### ingredient\_name

```gdscript
export var ingredient_name = ""
```

## Method Descriptions

### get\_class

```gdscript
func get_class()
```

