# Haptic Glove - Docs

This is the docs for the haptic glove project.

An API Docs is automatically generated but you can write articles. See [theme's docs](https://learn.netlify.app/en/cont/pages/). Write any articles in the ci/content/articles folder, it will automatically be copied whenever docs are generated.
